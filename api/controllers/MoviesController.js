/**
 * MoviesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  list: function(req, res) {
    Movies.find({}).exec(function(err, movies) {
      return res.view('movies/list', {movies: movies});
    })

  }

};
